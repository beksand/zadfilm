package com.beksand;

//Jedna z sieci kin chciałaby obliczyć jak duży sukces odniesie jej film. W tym celu potrzebuje program,
//        który sprawdzi ile dany film może zarobić w danym mieście.
//        Jeżeli film zaczyna się na "B" wybierze się na niego 3% ludzi z miasta.
//        Jeżeli film ma nazwę dłuższą niż 12 znaków pójdzie na niego 5% ludzi.
//        Jeżeli film ma nazwę krótszą niż 12 znaków pójdzie na niego 8% ludzi.
//        Jeżeli film ma w nazwie "ki" wybierze się na niego 6% ludzi.
//        Jeżeli cena biletu jest większa niż 40zł nie wybierze się na niego więcej niż 1% ludzi.
//
//        Ludność w miastach:
//        Warszawa 1744351
//        Kraków 761069
//        Łódź 700982
//        Wrocław 635759
//        Poznań 542348
//        Gdańsk 462249
//        Szczecin 405657
//        Bydgoszcz 355645
//        Lublin 340727
//        Katowice 299910
//        Białystok 295981
//        Gdynia 247478
//
//        Nazwy filmów:
//        Blade Runner 2049
//        Logan: Wolverine
//        Strażnicy Galaktyki vol. 2
//        Dunkierka
//        Twój Vincent
//        Obdarowani
//        Był sobie pies
//        Sztuka kochania. Historia Michaliny Wisłockiej
//        Piękna i Bestia
//        Spider-Man: Homecoming
//        Baby Driver
//        Bodyguard Zawodowiec
//        Król Artur: Legenda miecza
//        Azyl
//        Szybcy i wściekli 8
//        W starym, dobrym stylu
//        Piraci z Karaibów: Zemsta Salazara
//        Kingsman: Złoty krąg
//        Uciekaj!
//
//        Cena biletu to: ilość liter w nazwie * 2. Procenty mogą się sumować :)
//        Oblicz zarobki dla każdego miasta.

import java.util.*;

public class Main {

    public static void main(String[] args) {
        HashMap<String, Integer> city = new HashMap<>();
        city.put("Warszawa",1744351);
        city.put("Gdynia",247478);
        ArrayList<String> film = new ArrayList<>();
        film.add("Blade Runner 2049");
        film.add("Logan: Wolverine");
        int price=0;
        int kasa=0;
        double Coefficient = 0;
        Iterator<HashMap.Entry<String, Integer>> it = city.entrySet().iterator();
        while (it.hasNext()){

            Map.Entry<String, Integer> entry = it.next();
            for (int i = 0; i <film.size() ; i++) {
                kasa = (int) ( entry.getValue()*coefficient(i,film)*price(i,film));
                System.out.println("Dla "+entry.getKey()+" kasa z filmu "+ film.get(i)+" będzie "+kasa);
            }
        }


    }
    public static int price(int i, ArrayList<String> film){

        return film.get(i).length()*2;
    }
    public static double coefficient(int i, ArrayList<String> film){

        Character letter = (film.get(i)).charAt(0);
        if (letter.equals("B")){
            return 0.03;
        } else if (film.get(i).length()>12){
            return 0.05;
        } else if (film.get(i).length()<=12){
            return 0.08;
        } else if ((int)(price(i,film))>40){
            return 0.01;
        } else return 0;
    }
}
